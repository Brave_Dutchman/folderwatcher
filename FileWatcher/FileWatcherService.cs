﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FileWatcher
{
    public class FileWatcherService : BackgroundService
    {
        private readonly WatchSettings _settings;
        private readonly ILogger<FileWatcherService> _logger;
        private FileSystemWatcher[] _watchers;

        public FileWatcherService(IOptions<WatchSettings> options, ILogger<FileWatcherService> logger)
        {
            _settings = options.Value;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (_settings.Folders.Length > 0)
            {
                _initialize();

                await Task.Delay(Timeout.Infinite);
            }
        }

        private void _initialize()
        {
            var watchers = new List<FileSystemWatcher>();
            foreach (var folder in _settings.Folders)
            {
                _logger.LogInformation($"Watching folder: '{folder.Path}', and files: {string.Join(",", folder.Files.Select(f => f.Filename))}");

                var watcher = new FileSystemWatcher
                {
                    Path = folder.Path,
                    NotifyFilter = NotifyFilters.FileName
                };

                if (folder.Files.Any(f => f.ForEvents.Contains(Events.Created)))
                {
                    watcher.Created += (s, e) => _handleChange(folder, e.Name, Events.Created);
                    _logger.LogInformation("on Events.Created");
                }

                if (folder.Files.Any(f => f.ForEvents.Contains(Events.Changed)))
                {
                    watcher.Changed += (s, e) => _handleChange(folder, e.Name, Events.Changed);
                    _logger.LogInformation("on Events.Changed");
                }

                if (folder.Files.Any(f => f.ForEvents.Contains(Events.Deleted)))
                {
                    watcher.Deleted += (s, e) => _handleChange(folder, e.Name, Events.Deleted);
                    _logger.LogInformation("on Events.Deleted");
                }

                if (folder.Files.Any(f => f.ForEvents.Contains(Events.Renamed)))
                {
                    watcher.Renamed += (s, e) => _handleChange(folder, e.Name, Events.Renamed);
                    _logger.LogInformation("on Events.Renamed");
                }

                watcher.EnableRaisingEvents = true;
            }

            _watchers = watchers.ToArray();
        }

        private void _handleChange(WatchFolderInfo folder, string changedFilename, Events trigger)
        {
            var file = folder.Files.FirstOrDefault(f => f.Filename == changedFilename);
            if (file != default)
            {
                if (file.ForEvents.Contains(trigger))
                {
                    _logger.LogInformation($"File '{changedFilename}' triggerd' {trigger}'");
                    _runScript(file);
                }
                else
                {
                    _logger.LogDebug($"Invalid Trigger for file '{changedFilename}'");
                }
            }
        }

        private void _runScript(WatchFileInfo file)
        {
            _logger.LogDebug($"Running script '{file.ScriptPath}'");

            lock (file)
            {
                var p = new Process
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "powershell.exe",
                        Arguments = $"-NoProfile -ExecutionPolicy unrestricted -file \"{file.ScriptPath}\"",
                        UseShellExecute = true,
                    }
                };

                p.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);
                p.ErrorDataReceived += (sender, args) => Console.WriteLine(args.Data);

                p.Start();
                p.WaitForExit();
            }
        }

        public override void Dispose()
        {
            if (_watchers != null)
            {
                foreach (var watcher in _watchers)
                {
                    watcher.Dispose();
                }

                _watchers = null;
            }

            base.Dispose();
        }
    }
}

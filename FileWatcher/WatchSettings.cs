﻿using System;

namespace FileWatcher
{
    public class WatchSettings
    {
        public WatchFolderInfo[] Folders { get; set; } = Array.Empty<WatchFolderInfo>();
    }
}

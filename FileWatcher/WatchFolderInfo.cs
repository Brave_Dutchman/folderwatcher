﻿using System;

namespace FileWatcher
{
    public class WatchFolderInfo
    {
        public string Path { get; set; }

        public string Filter { get; set; }

        public WatchFileInfo[] Files { get; set; } = Array.Empty<WatchFileInfo>();
    }
}

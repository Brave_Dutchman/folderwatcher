﻿namespace FileWatcher
{
    public class WatchFileInfo
    {
        public string Filename { get; set; }

        public string ScriptPath { get; set; }

        public Events[] ForEvents { get; set; }
    }
}

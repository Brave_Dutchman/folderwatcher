﻿namespace FileWatcher
{
    public enum Events
    {
        Created = 0,
        Changed = 1,
        Deleted = 2,
        Renamed = 3
    }
}

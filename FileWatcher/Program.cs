﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using System.Security.Permissions;
using System.Threading.Tasks;

namespace FileWatcher
{
    internal class Program
    {
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .ConfigureHostConfiguration(_configureHostConfiguration)
                .ConfigureAppConfiguration(_configureAppConfiguration)
                .ConfigureServices(_configureServices)
                .ConfigureLogging(_configureLogging);

            await builder
                .Build()
                .RunAsync();
        }

        private static void _configureHostConfiguration(IConfigurationBuilder configBuilder)
        {
            configBuilder.AddJsonFile("hostsettings.json", optional: true);
        }

        private static void _configureAppConfiguration(HostBuilderContext hostingContext, IConfigurationBuilder config)
        {
            config.AddEnvironmentVariables();

            config.AddJsonFile("appsettings.json", optional: false);
            config.AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", optional: true);
        }

        private static void _configureServices(HostBuilderContext hostingContext, IServiceCollection services)
        {
            var startup = new Startup(hostingContext.Configuration, hostingContext.HostingEnvironment);
            startup.ConfigureServices(services);
        }

        private static void _configureLogging(HostBuilderContext hostingContext, ILoggingBuilder logging)
        {
            logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));

            logging.AddConsole();
            if (hostingContext.HostingEnvironment.IsDevelopment())
            {
                logging.AddDebug();
            }
        }
    }
}
